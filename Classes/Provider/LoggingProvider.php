<?php

namespace WEBprofil\WpT3monitoringClient\Provider;

use T3Monitor\T3monitoringClient\Provider\DataProviderInterface;
use TYPO3\CMS\Core\Core\Environment;

class LoggingProvider implements DataProviderInterface
{

    /**
     * @param array $data
     * @return array
     */
    public function get(array $data): array
    {
        if ((int)$GLOBALS['TYPO3_CONF_VARS']['SYS']['belogErrorReporting'] !== 31061 && (string)Environment::getContext() === 'Production') {
            $data['extra']['danger']['Logging'] = 'belogErrorReporting is not set to 31061';
        } else {
            $data['extra']['success']['Logging'] = 'belogErrorReporting is set to 31061';
        }
        return $data;
    }
}
