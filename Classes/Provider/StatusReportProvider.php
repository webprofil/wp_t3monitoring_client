<?php

namespace WEBprofil\WpT3monitoringClient\Provider;

/*
 * This file is part of the t3monitoring_client extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Report\InstallStatusReport;
use TYPO3\CMS\Install\Report\SecurityStatusReport;
use TYPO3\CMS\Reports\Report\Status as Stati;
use TYPO3\CMS\Reports\Status;

/**
 * Class StatusReportProvider
 */
class StatusReportProvider extends \T3Monitor\T3monitoringClient\Provider\StatusReportProvider
{

    /**
     * @param array $data
     * @return array
     */
    public function get(array $data): array
    {
        if (ExtensionManagementUtility::isLoaded('reports')) {
            $this->initialize();
            $statusReport = GeneralUtility::makeInstance(Stati\Status::class);
            $statusCollection = $statusReport->getSystemStatus();

            $severityConversion = [
                Status::INFO => 'info',
                Status::WARNING => 'warning',
                Status::ERROR => 'danger',
                Status::OK => 'success'
            ];

            foreach ($statusCollection as $statusProvider => $providerStatuses) {
                /** @var $status Status */
                foreach ($providerStatuses as $status) {
                    $title = sprintf('%s - %s', $status->getTitle(), $status->getValue());
                    $convertedSeverity = $severityConversion[$status->getSeverity()];
                    $data['extra'][$convertedSeverity][$title] = $status->getMessage();
                }
            }
        }

        return $data;
    }

}
