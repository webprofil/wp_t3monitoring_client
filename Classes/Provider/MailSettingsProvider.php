<?php

namespace WEBprofil\WpT3monitoringClient\Provider;

use T3Monitor\T3monitoringClient\Provider\DataProviderInterface;
use WEBprofil\WpT3monitoringClient\Utility\GeneralUtility;

class MailSettingsProvider implements DataProviderInterface
{
    /**
     * @param array $data
     * @return array
     */
    public function get(array $data): array
    {
        if ($GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport'] !== '/usr/sbin/sendmail -t -i' && GeneralUtility::isTimmeServer()) {
            $data['extra']['danger']['Mail Transport'] = 'Mail transport is not set to sendmail -t -i';
        } else {
            $data['extra']['success']['Mail Transport'] = 'Mail transport is set to sendmail -t -i';
        }
        return $data;
    }
}
