<?php

namespace WEBprofil\WpT3monitoringClient\Provider;

use Doctrine\DBAL\Connection as ConnectionAlias;
use Doctrine\DBAL\Exception;
use T3Monitor\T3monitoringClient\Provider\DataProviderInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class OldUserProvider implements DataProviderInterface
{

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function get(array $data): array
    {
        // Check if there are fe_users
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('be_users');

        $usernames = [
            'wpbb',
            'wpgp',
            'wpmm',
            'wpmp',
            'wpto',
            'wpjp'
        ];

        $queryBuilder
            ->getRestrictions()
            ->removeByType(HiddenRestriction::class);
        // find all be_users that have username from array, ignore hidden
        $oldUsers = $queryBuilder
            ->select('username')
            ->from('be_users')
            ->where($queryBuilder->expr()->in('username',
                $queryBuilder->createNamedParameter($usernames, ConnectionAlias::PARAM_STR_ARRAY)))
            ->executeQuery()
            ->fetchAllAssociative();

        // if there are old users, add them to the data array
        if (!empty($oldUsers)) {
            $data['extra']['danger']['Old users found'] = 'The following old users were found: ' . implode(', ',
                    array_column($oldUsers, 'username'));
        } else {
            $data['extra']['success']['No old users found'] = '';
        }

        return $data;
    }
}
