<?php

namespace WEBprofil\WpT3monitoringClient\Controller;

use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use WEBprofil\WpT3monitoringClient\Client;

class MonitoringController extends ActionController
{
    protected ModuleTemplateFactory $moduleTemplateFactory;

    public function __construct(
        ModuleTemplateFactory $moduleTemplateFactory
    ) {
        $this->moduleTemplateFactory = $moduleTemplateFactory;
    }

    public function listAction(): ResponseInterface
    {
        $client = GeneralUtility::makeInstance(Client::class);
        $data = $client->collectData();
        $this->view->assign('data', $data);
        $moduleTemplate = $this->moduleTemplateFactory->create($this->request);
        $moduleTemplate->setContent($this->view->render());
        unset($GLOBALS['TSFE']);
        return $this->htmlResponse($moduleTemplate->renderContent());
    }
}
