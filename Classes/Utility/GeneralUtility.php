<?php /** @noinspection GrazieInspection */

namespace WEBprofil\WpT3monitoringClient\Utility;

class GeneralUtility
{
    public static function isTimmeServer()
    {
        // get ip of server
        $ip = $_SERVER['SERVER_ADDR'];

        // get ip of mars.webprofil.at
        $ipMars = gethostbyname('mars.webprofil.at');

        return $ip !== $ipMars;
    }
}