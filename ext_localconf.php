<?php
defined('TYPO3_MODE') || die('Access denied.');
$okayProvider =[];
foreach ($GLOBALS['TYPO3_CONF_VARS']['EXT']['t3monitoring_client']['provider'] as $provider) {
    if ($provider !== \T3Monitor\T3monitoringClient\Provider\StatusReportProvider::class) {
        $okayProvider[] = $provider;
    }
}
$GLOBALS['TYPO3_CONF_VARS']['EXT']['t3monitoring_client']['provider'] = $okayProvider;
$GLOBALS['TYPO3_CONF_VARS']['EXT']['t3monitoring_client']['provider'][] = \WEBprofil\WpT3monitoringClient\Provider\MailSettingsProvider::class;
$GLOBALS['TYPO3_CONF_VARS']['EXT']['t3monitoring_client']['provider'][] = \WEBprofil\WpT3monitoringClient\Provider\LoggingProvider::class;
$GLOBALS['TYPO3_CONF_VARS']['EXT']['t3monitoring_client']['provider'][] = \WEBprofil\WpT3monitoringClient\Provider\StatusReportProvider::class;
$GLOBALS['TYPO3_CONF_VARS']['EXT']['t3monitoring_client']['provider'][] = \WEBprofil\WpT3monitoringClient\Provider\OldUserProvider::class;
