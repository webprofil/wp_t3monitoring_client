<?php

$EM_CONF['wp_t3monitoring_client'] = [
    'title' => 'Monitoring Client',
    'description' => '',
    'category' => 'plugin',
    'author' => 'Benjamin Butschell',
    'author_email' => 'bb@webprofil.at',
    'state' => 'stable',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.9.99',
            't3monitoring_client' => '9.0.0-9.9.99',
        ],
        'conflicts' => [],
        'suggests' => [
            't3monitoring_client' => '9.0.0-9.9.99'
        ],
    ],
];
