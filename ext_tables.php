<?php
defined('TYPO3') || die();

(static function() {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'WpT3monitoringClient',
        'tools',
        'monitoring',
        '',
        [
            \WEBprofil\WpT3monitoringClient\Controller\MonitoringController::class => 'list',
            
        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:wp_t3monitoring_client/Resources/Public/Icons/user_mod_monitoring.svg',
            'labels' => 'LLL:EXT:wp_t3monitoring_client/Resources/Private/Language/locallang_monitoring.xlf',
        ]
    );

})();
